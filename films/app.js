const express = require("express"); 
const app = express(); 
const cors = require('cors'); 
const bodyParser = require("body-parser"); 
const Film = require("./models/film_model");
const jwt = require("jsonwebtoken");

app.use(bodyParser.json());
app.use(cors());

app.get("/", (req, res) => {
  res.json({ msg: "films" });
});

app.get("/api/v1/films", async (req, res) => {
  const films = await Film.find({});
  res.json(films);
});

app.post("/api/v1/login", (req, res) =>{
  const user = {
    username: req.body.username
  }
  jwt.sign({ user }, 'secretkey', (err, token) => {
    res.json({
      token
    })
  }); 
});

app.put("/api/v1/films", verifyToken, async (req, res) => {
  var found = false; 
  const films = await Film.find({}); 
  const film = new Film({
    name: req.body.name,
    rating: Number.parseInt(req.body.rating) 
  });
  var f; 
  for (var i = 0; i < films.length; i++){
    if (film.name === films[i].name){
      films[i].rating = film.rating; 
      f = films[i]; 
      console.log("trained"); 
      found = true; 
    }
  }
  if (found == true){
    const savedFilm = await f.save(); 
    res.json(savedFilm); 
    console.log("Hello From the Put Method");
  }
  else{
    console.log("Couldn't Find That Entry");
  }
});

app.post("/api/v1/films", verifyToken, async (req, res) => {
  const film = new Film({
    name: req.body.name,
    rating: Number.parseInt(req.body.rating) 
  });
  const savedFilm = await film.save(); 
  res.json(savedFilm); 
});

function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization']; 
  if (typeof bearerHeader !== 'undefined'){
    const bearerToken = bearerHeader.split(' ')[1]; 
    jwt.verify(bearerToken, 'secretkey', (err, authData) =>{
      if (err){
        res.sendStatus(403); 
      }else{
        next();
      }
    })
  } else{
    res.sendStatus(403); //ended here on bottom of page 2
    console.log("Error found here");
  }
}


module.exports = app; 
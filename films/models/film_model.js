const mongoose = require("mongoose");
const Schema = mongoose.Schema; 

const FilmSchema = new Schema({
    rating: Number,
    name: String,
})

module.exports = mongoose.model("Film", FilmSchema); 
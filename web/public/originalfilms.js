var API = (() => {

    var movies = [];
    var addedMovie = false; 
    var formerLength = movies.length; 

    var createFilm = () => {
        var newAddition = document.getElementById("filmBox"); 
        if (newAddition.value != "" && newAddition.value != " " && !(movies.includes(newAddition.value))){
            movies.push(newAddition.value); 
            addedMovie = true; 
            newAddition.value = ""; 
            console.log("Success");
        }
        return false; 
    }

    var getFilms = () => {
        console.log(movies);
        var movieTable = document.getElementById("createdMovies");
        if(movies.length != 0 && addedMovie == true){
            movieTable.style.display = "block";
            for (var i = formerLength+1; i <= movies.length; i++){
                var row = movieTable.insertRow(i);
                var firstCell = row.insertCell(0); 
                var secondCell = row.insertCell(1); 
                var thirdCell = row.insertCell(2);
                secondCell.innerHTML = movies[i-1]; 
                thirdCell.innerHTML = "★★★★★"; 
                addedMovie = false;
            }  
            formerLength = movies.length; 
        }
        return false; 
    }


    return {
        createFilm,
        getFilms
    }

})();
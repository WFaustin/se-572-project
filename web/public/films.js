var API = (() => {
    var jwtToken; 
    var addedMovie = false; 
    var formerLength = 0; 


    var login = () =>{
        const val = document.getElementById("loginBox").value;
        var noWhiteSpaceAddition = val.toString().replace(/^\s+/, '').replace(/\s+$/, '');
        const name = "Widchard"; 
        if (noWhiteSpaceAddition == ''){
            console.log("Nothing in the textbox");
        }
        else if (val == name){
            try{
                fetch("http://192.168.1.152:8080/api/v1/login", {
                    method: 'POST', 
                    body: JSON.stringify({
                        username: val
                    }), 
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json', 
                    }
                }).then(resp => resp.json())
                    .then(data => {
                        jwtToken = data.token;
                        console.log("Login Successful!");
                    });
            } catch (e) {
                console.log(e);
                console.log('---------------------------------'); 
                console.log("Login Failed!"); 
            }
        }
        else{
            console.log("Try again with proper username");
        }
        return false;
    }

    var createFilm = () => {
        var newAddition = document.getElementById("filmBox"); 
        var newRating = document.getElementById("ratingBox"); 
        //Variable that holds the title without whitespace
        var noWhiteSpaceAddition = newAddition.value.toString().replace(/^\s+/, '').replace(/\s+$/, '');
        //Variable that makes sure the rating is valid. 
        var numberRating = Number.parseInt(newRating.value); 
        
        if (noWhiteSpaceAddition == ''){
            console.log("The title that you submitted only consists of whitespaces. Please submit a valid title."); 
        }
        else if(Number.isNaN(numberRating)){
            console.log("The item you entered is not a valid number, please sumbit a number between 1 - 5 in NUMERICAL form."); 
        }
        else if(numberRating > 5 || numberRating < 1){
            console.log("The number that you entered is not in the specified range, please sumbit a number between 1 - 5 in NUMERICAL form."); 
        }
        
        else{
            try{
                fetch("http://192.168.1.152:8080/api/v1/films", {
                    method: 'POST',
                    body: JSON.stringify({
                        name: newAddition.value,
                        rating: numberRating
                    }), 
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + jwtToken
                    }
                }).then(resp => {
                    setTimeout(function () {
                        if (resp.status == 200){
                            console.log("Input was logged along with credentials");
                            console.log("Success");
                        } else{
                            console.log("Something didn't go quite right, please try again.")
                        }
                    })
                })
            } catch (e) {
                console.log(e);
                console.log("-----------------------------------"); 
            }
            addedMovie = true; 
            newAddition.value = ""; 
            newRating.value = "";
        }
        return false; 
    }

    var getFilms = () => {
        var movieTable = document.getElementById("createdMovies");
        if (addedMovie == true){
            movieTable.style.display = "block";
            try{
                fetch("http://192.168.1.152:8080/api/v1/films", {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json', 
                        'Content-Type': 'application/json',
                    }
                }).then(resp => resp.json())
                    .then(results => {
                    for (var i = 1; i <= results.length; i++){
                        if (i <= formerLength){
                            var row = movieTable.rows[i];
                            var firstCell = row.cells[0]; 
                            var secondCell = row.cells[1];
                            var thirdCell = row.cells[2];
                            firstCell.innerHTML = "#" + i; 
                            secondCell.innerHTML = results[i-1].name;
                            thirdCell.innerHTML = "";
                            for (var j = 0; j < Number.parseInt(results[i-1].rating); j++){
                                thirdCell.innerHTML += "*";
                            } 
                        }
                        else{
                            var row = movieTable.insertRow(i);
                            var firstCell = row.insertCell(0); 
                            var secondCell = row.insertCell(1); 
                            var thirdCell = row.insertCell(2);
                            firstCell.innerHTML = "#" + i; 
                            secondCell.innerHTML = results[i-1].name;
                            for (var j = 0; j < Number.parseInt(results[i-1].rating); j++){
                                //thirdCell.innerHTML += "✵";
                                thirdCell.innerHTML += "*";
                            }
                            formerLength += 1;
                        }
                        //thirdCell.innerHTML =  Number.parseInt(results[i-1].rating) + "/5 stars"; 
                        addedMovie = false; 
                    }
                }); 
            } catch(e){
                console.log(e);
                console.log("----------------------------------------------"); 
            }
            addedMovie = false;
        }
        return false; 
    }


    var changeRating = () =>{
        var newAddition = document.getElementById("filmBox"); 
        var newRating = document.getElementById("ratingBox"); 
        //Variable that holds the title without whitespace
        var noWhiteSpaceAddition = newAddition.value.toString().replace(/^\s+/, '').replace(/\s+$/, '');
        //Variable that makes sure the rating is valid. 
        var numberRating = Number.parseInt(newRating.value); 
        
        if (noWhiteSpaceAddition == ''){
            console.log("The title that you submitted only consists of whitespaces. Please submit a valid title."); 
        }
        else if(Number.isNaN(numberRating)){
            console.log("The item you entered is not a valid number, please sumbit a number between 1 - 5 in NUMERICAL form."); 
        }
        else if(numberRating > 5 || numberRating < 1){
            console.log("The number that you entered is not in the specified range, please sumbit a number between 1 - 5 in NUMERICAL form."); 
        }
        else{
            try{
                fetch("http://192.168.1.152:8080/api/v1/films", {
                    method: 'PUT',
                    body: JSON.stringify({
                        name: newAddition.value,
                        rating: numberRating
                    }), 
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + jwtToken
                    }
                })
                .then(resp => {
                    setTimeout(function () {
                        if (resp.status == 200){
                            console.log("Input was logged along with credentials");
                            console.log("Success");
                        } else{
                            console.log("Something didn't go quite right, please try again.")
                        }
                    })
                    
                   console.log("Hello from the changeRating"); 
                })
            } catch (e) {
                console.log(e);
                console.log("-----------------------------------"); 
            }
            //console.log("Hello from the changeRating"); 
            addedMovie = true; 
            newAddition.value = ""; 
            newRating.value = "";
        }
        return false; 
    }

    return {
        login, 
        createFilm,
        getFilms,
        changeRating
    }

})();
var API = (() => {

    var addedMovie = false; 
    var formerLength = 0; 

    var createFilm = () => {
        var newAddition = document.getElementById("filmBox"); 
        if (newAddition.value != "" && newAddition.value != " " && !(movies.includes(newAddition.value))){
            try{
                fetch("http://localhost:3001/api/v1/films", {
                    method: 'POST',
                    body: JSON.stringify({
                        name: newAddition.value
                    }), 
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                });
            } catch (e) {
                console.log(e);
                console.log("-----------------------------------"); 
            }
            addedMovie = true; 
            console.log("Success");
            newAddition.value = ""; 
        }
        return false; 
    }

    var getFilms = () => {
        var movieTable = document.getElementById("createdMovies");
        try{
            fetch("http://localhost:3001/api/v1/films", {
                method: 'GET',
                headers: {
                    'Accept': 'application/json', 
                    'Content-Type': 'application/json'
                }
            }).then(results => {
                for (var i = formerLength+1; i <= results.length; i++){
                    var row = movieTable.insertRow(i);
                    var firstCell = row.insertCell(0); 
                    var secondCell = row.insertCell(1); 
                    var thirdCell = row.insertCell(2);
                    secondCell.innerHTML = results[i-1]; 
                    thirdCell.innerHTML = "★★★★★"; 
                    addedMovie = false;
                }  
            });
            formerLength = movies.length; 
        } catch(e){
            console.log(e);
            console.log("----------------------------------------------"); 
        }
        return false; 
    }


    return {
        createFilm,
        getFilms
    }

})();
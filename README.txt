Widchard Faustin
9/5/2020

README for the App portion of the Project


This App uses Android Studio. In order to use it, you need to download Android Studio, and either have an Android Device that you can connect
to your computer using a USB or an Android Emulator (will cover later). 

After unzipping the project/pulling the repository from GitHub, the first time when opening up this project using Android Studio, use the 
Import choice and navigate to the directory of the app to import it. Then, you should be able to modify code with little to no issues (this
won't be the case if there were issues with your installation of Android Studio). 

To run the application, follow the steps at this link: https://developer.android.com/training/basics/firstapp/running-app. The documentation
for android studio is more than sufficient. This includes downloading an emulator. 
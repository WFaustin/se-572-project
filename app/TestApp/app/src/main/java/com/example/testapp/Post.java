package com.example.testapp;

public class Post {

    private int rating;
    private String name;
    public String token;

    public Post(int rating, String name) {
        this.rating = rating;
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public String getLogin(){
        return token;
    }
}

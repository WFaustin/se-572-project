package com.example.testapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface SE572API {

    @GET("api/v1/films")
    Call<List<Post>> getPosts();

    @POST("api/v1/films")
    Call<Post> createPost(@Header ("Authorization") String header, @Body Post post);

    @PUT("api/v1/films")
    Call<Post> updateRating(@Header ("Authorization") String header, @Body Post post);

    @POST("api/v1/login")
    Call<Post> loggedIn();

}

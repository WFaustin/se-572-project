package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import retrofit2.Call;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplayMessageActivity extends AppCompatActivity {

    private EditText movieTitle;
    private EditText movieRating;
    private TextView testOutput;
    private LoginTokenSingleton lts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        movieTitle = findViewById(R.id.movieTitleInput);
        movieRating = findViewById(R.id.movieRatingInput);
        testOutput = findViewById(R.id.testOutput);
        lts = LoginTokenSingleton.getInstance();

    }

    public void createPost(View view){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.152:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SE572API movieAPI = retrofit.create(SE572API.class);
        String s = "Bearer " + lts.getLoginToken();
        String [] g = movieTitle.getText().toString().split(" ");
        boolean open = true;
        boolean opennum = true;
        if (movieTitle.getText().toString().contentEquals("")){
            open = false;
        }
        if (g.length == 0){
            open = false;
        }
        for (int i = 0; i < g.length; i++){
            if (g[i].length() > 0){
                open = true;
                break;
            }
        }
        int v;
        try {
            v = Integer.parseInt(movieRating.getText().toString());
            opennum = true;
            if (v > 5 || v <= 0){
                opennum = false;
            }
        } catch (NumberFormatException e) {
            opennum = false;
            v = 1;
        }

        if (open == true && opennum == true && lts.getLoginToken() != null) {
            Post post = new Post(Integer.parseInt(movieRating.getText().toString()), movieTitle.getText().toString());
            Call<Post> call = movieAPI.createPost(s, post);

            call.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {
                    if (!response.isSuccessful()){
                        return;
                    }

                    testOutput.setText("Submitted");
                    lts.setAddedMovie(true);
                }

                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                    testOutput.setText(t.getMessage());
                }
            });
        }
        else if(open == false){
            testOutput.setText("Please Fill The Title");
        }
        else if(lts.getLoginToken() == null){
            testOutput.setText("Please Login First");
        }
        else if(opennum == false && v == 1){
            testOutput.setText("Please put the number in the rating textbox");
        }
        else{
            testOutput.setText("Please put a number in the rating textbox that is in the range of 1 and 5");
        }
    }

    public void updatePost(View view){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.152:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SE572API movieAPI = retrofit.create(SE572API.class);
        String s = "Bearer " + lts.getLoginToken();
        boolean open = true;
        boolean opennum = true;
        if (movieTitle.getText().toString().contentEquals("")){
            open = false;
        }
        String [] g = movieTitle.getText().toString().split(" ");
        if (g.length == 0){
            open = false;
        }
        for (int i = 0; i < g.length; i++){
            if (g[i].length() > 0){
                open = true;
                break;
            }
        }
        int v;
        try {
            v = Integer.parseInt(movieRating.getText().toString());
            opennum = true;
            if (v > 5 || v <= 0){
                opennum = false;
            }
        } catch (NumberFormatException e) {
            opennum = false;
            v = 1;
        }

        if (open == true && opennum == true && lts.getLoginToken() != null) {
            Post post = new Post(Integer.parseInt(movieRating.getText().toString()), movieTitle.getText().toString());
            Call<Post> call = movieAPI.updateRating(s, post);

            call.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {
                    if (!response.isSuccessful()){
                        return;
                    }

                    testOutput.setText("Submitted");
                    lts.setAddedMovie(true);
                }

                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                    testOutput.setText(t.getMessage());
                }
            });
        }
        else if(open == false){
            testOutput.setText("Please Fill The Title");
        }
        else if(lts.getLoginToken() == null){
            testOutput.setText("Please Login First");
        }
        else if(opennum == false && v == 1){
            testOutput.setText("Please put the number in the rating textbox");
        }
        else{
            testOutput.setText("Please put a number in the rating textbox that is in the range of 1 and 5");
        }
    }

}
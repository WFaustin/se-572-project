package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private boolean hasLoggedIn;
    private TextView textViewResult;
    private TextView textViewLogin;
    private LoginTokenSingleton lts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResult = findViewById(R.id.API);
        textViewLogin = findViewById(R.id.LoggedInMessage);
        hasLoggedIn = false;
        lts = LoginTokenSingleton.getInstance();
    }

    public void sendMessage(View view){
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        startActivity(intent);
    }

    public void loginCorrectly(View view){
        textViewLogin.setText("");
        TextView password = findViewById(R.id.PasswordBox);
        String s = password.getText().toString();
        if (s.contentEquals("Widchard")){
            /*
            textViewLogin.setText("LogIn Successful");
            hasLoggedIn = true;
            */

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://192.168.1.152:8080/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            SE572API movieAPI = retrofit.create(SE572API.class);

            Call<Post> all = movieAPI.loggedIn();

            all.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {

                    if (!response.isSuccessful()){
                        textViewLogin.setText("Code: " + response.code() + " Login Unsuccessful.");
                        return;
                    }
                    Post p = response.body();
                    textViewLogin.setText("LogIn Successful");
                    lts.setLoginToken(p.token);
                    hasLoggedIn = true;

                }

                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                    textViewLogin.setText(t.getMessage());
                }
            });
        }
        else{
            textViewLogin.setText("Incorrect Password. The Password is Widchard");
        }
    }

    public void getMovieList(View view){
        textViewResult.setText("");
        if (lts.getLoginToken() != null){
            hasLoggedIn = true;
        }
        if (hasLoggedIn == true && lts.isAddedMovie() == true){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://192.168.1.152:8080/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            SE572API movieAPI = retrofit.create(SE572API.class);

            Call<List<Post>> all = movieAPI.getPosts();

            all.enqueue(new Callback<List<Post>>() {
                @Override
                public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                    if (!response.isSuccessful()){
                        textViewResult.setText("Code: " + response.code());
                        return;
                    }

                    List<Post> posts = response.body();

                    for (Post post : posts){
                        String content = "";
                        content += "Movie Title: " + post.getName() + "\n" + "Movie Rating: " + post.getRating() + " stars \n\n";

                        textViewResult.append(content);
                    }
                }

                @Override
                public void onFailure(Call<List<Post>> call, Throwable t) {
                    textViewResult.setText(t.getMessage());
                }
            });
        }
        else if(hasLoggedIn == false){
            textViewResult.setText("You haven't logged in yet. The password is Widchard.");
        }
        else{
            textViewResult.setText("Please Add a Movie First.");
        }
    }
}
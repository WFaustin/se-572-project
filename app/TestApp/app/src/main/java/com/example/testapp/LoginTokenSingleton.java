package com.example.testapp;

public class LoginTokenSingleton {

    private static volatile LoginTokenSingleton lts;

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    private String loginToken;

    public boolean isAddedMovie() {
        return addedMovie;
    }

    public void setAddedMovie(boolean addedMovie) {
        this.addedMovie = addedMovie;
    }

    private boolean addedMovie = false;

    private LoginTokenSingleton(){
        loginToken = null;
    }

    public static LoginTokenSingleton getInstance(){
        if (lts == null){
            lts = new LoginTokenSingleton();
        }
        return lts;
    }



}
